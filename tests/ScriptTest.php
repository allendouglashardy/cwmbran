<?php

require_once __DIR__.'/../script.php';

use PHPUnit\Framework\TestCase;

/**
 * Class ScriptTest
 */
class ScriptTest extends TestCase{

    public function testMixedArray()
    {
        $arr = ["x"=>1,'y'=>2,0=>4];

        $result = $this->execute($arr,function () use ($arr){
            return json_decode(json_encode($arr),true);
        });

        $this->assertArrayHasKey('x',$result);
        $this->assertArrayHasKey('y',$result);
        $this->assertArrayHasKey('0',$result);
    }

    public function testStandardArray()
    {
        $arr = [0,2,4];

        $result = $this->execute($arr,function () use ($arr){
            return json_decode(json_encode($arr),true);
        });

        $this->assertArrayHasKey('0',$result);
        $this->assertArrayHasKey('1',$result);
        $this->assertArrayHasKey('2',$result);
    }

    public function testAssocInput()
    {
        $arr = [
            'A'=>1,
            'B'=>2,
            'C'=>3
        ];

        $result = $this->execute($arr,function () use ($arr){
            return json_decode(json_encode($arr),true);
        });

        $this->assertArrayHasKey('A',$result);
        $this->assertArrayHasKey('B',$result);
        $this->assertArrayHasKey('C',$result);
    }

    public function testEmptyArray()
    {
        $arr = [];

        $result = $this->execute($arr,function () use ($arr){
            return json_decode(json_encode($arr),true);
        });

        $this->assertTrue(empty($result));
    }

    protected function execute($array,$closure)
    {
        return array_build($array,$closure);
    }
}