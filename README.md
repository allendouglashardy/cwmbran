# Test For Php MySql Javascript

## Installation

-- Clone this repository
-- From the root of the repo run `composer update`

## Question One: Php

See /scripts.php for answer

### To run unit tests

run vendor/bin/phpunit tests

## Question Two : MySQL

see /schema.sql

## Question 3 : Javascript

see /words.html