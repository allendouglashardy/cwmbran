
CREATE TABLE IF NOT EXISTS `booking`(
  `id` INT NOT NULL AUTO_INCREMENT ,
  `lead_name` VARCHAR(56) NOT NULL,
  `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME ON UPDATE CURRENT_TIMESTAMP,
  `start_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
  `nights` INT NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `reservation`(
  `id` INT NOT NULL AUTO_INCREMENT ,
  `booking_id` INT NOT NULL,
  `room_type` VARCHAR(28) NOT NULL,
  `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

ALTER TABLE `reservation`
ADD FOREIGN KEY (`booking_id`) REFERENCES booking(`id`);

CREATE TABLE IF NOT EXISTS `invoice`(
  `id` INT NOT NULL AUTO_INCREMENT ,
  `reservation_id` INT NOT NULL,
  `amount_in_pence` INT NOT NULL DEFAULT 0,
  `paid` TINYINT DEFAULT 0,
  `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

ALTER TABLE `invoice`
ADD FOREIGN KEY(`reservation_id`) REFERENCES reservation(`id`);

CREATE TABLE IF NOT EXISTS `invoice_responsibility`(
  `id` INT NOT NULL AUTO_INCREMENT ,
  `guest_id` INT NULL,
  `company_id` INT NULL,
  `trader_id` INT NULL,
  PRIMARY KEY (`id`)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `guest`(
  `id` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `company`(
  `id` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `trader`(
  `id` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
)ENGINE=InnoDB;

ALTER TABLE `invoice_responsibility`
ADD FOREIGN KEY(`invoice_id`) REFERENCES invoice(`id`);

ALTER TABLE `invoice_responsibility`
ADD FOREIGN KEY(`guest_id`) REFERENCES guest(`id`);

ALTER TABLE `invoice_responsibility`
ADD FOREIGN KEY(`company_id`) REFERENCES company(`id`);

ALTER TABLE `invoice_responsibility`
ADD FOREIGN KEY(`trader_id`) REFERENCES trader(`id`);


