<?php

require __DIR__.'/vendor/autoload.php';

/**
 * @param array $array
 * @param Closure $closure
 * @return array
 */
function array_build($array=[],Closure $closure) : array
{
    $result = $closure();

    return ((is_array($result)) && array_values($result) !==$result)
        ? $result
        : json_decode(json_encode($array),true);
}

